import React from 'react';
import { useState } from 'react';
import Modal from "./Modal";
import {useHistory, Link} from 'react-router-dom'


function Breed(props){

    const [show, setShow] = useState(false);

    //manipulacija urla koristeci react router dom
    let history = useHistory();
    function handleBack(){
      history.push("/");
    }
    
    return( 
        <div className="flex-container breeds">
          <div className ="flex-child text-white text-capitalize font-weight-bold" 
            onClick={function(event){setShow(true); }}>
          <Link className="text-white" to={"/" + props.breed}> {props.breed} </Link></div> 
          <Modal title={props.breed}
           onClose={function(event){setShow(false); handleBack()}} show={show} >
            <p>Known sub-breeds of this breed :</p>
          </Modal>
        </div>
    )
}
export default Breed