import React, { useEffect } from "react";
import ReactDOM from "react-dom";
import { CSSTransition } from "react-transition-group";
import "./Modal.css";
import axios from 'axios';

const Modal = props => {

  const [photos, setPhotos] = React.useState([]);

  const [subBreeds, setSubBreed] = React.useState([]);

  //dohvati slike i subBreedove
  useEffect(() => {
    const httpOfBreed = "https://dog.ceo/api/breed/"+ props.title + "/images"
    const httpOfSubBreed = "https://dog.ceo/api/breed/" + props.title + "/list"

      const requestOne = axios.get(httpOfBreed);
      const requestTwo = axios.get(httpOfSubBreed);
      axios.all([requestOne, requestTwo])
      .then(axios.spread((...responses) => {
        const responseOne =responses[0].data.message
        const responseTwo =responses[1].data.message
        setPhotos(responseOne)
        setSubBreed(responseTwo)
      }))
      .catch(function (error) {
        console.log(error);
      });
    }, []);

  const closeOnEscapeKeyDown = e => {
    if ((e.charCode || e.keyCode) === 27) {
      props.onClose();
    }
  };

  useEffect(() => {
    document.body.addEventListener("keydown", closeOnEscapeKeyDown);
    return function cleanup() {
      document.body.removeEventListener("keydown", closeOnEscapeKeyDown);
    };
  }, []);

  //postavi stringove za ispis 
  var stringSubBreeds = ""
  for(var i = 0; i < subBreeds.length; i++){
    stringSubBreeds = stringSubBreeds + subBreeds[i] + ", "
  }
  if(stringSubBreeds === ""){
    stringSubBreeds = "This dog breed has no sub-breeds  "
  }
  stringSubBreeds = stringSubBreeds.slice(0, -2)

  return ReactDOM.createPortal(
    <CSSTransition
      in={props.show}
      unmountOnExit
      timeout={{ enter: 100, exit: 300 }}
    >
      <div className="modal" onClick={props.onClose}>
        <div className="modal-content" onClick={e => e.stopPropagation()}>
          <div className="modal-header">
            <h4 className="modal-title display-3 text-capitalize">{props.title}</h4>
          </div>
          <div className="modal-body font-weight-bold"><div>{props.children} {stringSubBreeds}</div>
            <img src={photos[0]} width="450" height="450"/>
          </div>
          <div className="modal-footer">
            <button onClick={props.onClose} className="button">
              Close
            </button>
          </div>
        </div>
      </div>
    </CSSTransition>,
    document.getElementById("root")
  );
};

export default Modal;