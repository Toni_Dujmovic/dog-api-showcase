import './App.css';
import { QueryClient, QueryClientProvider, useQuery } from 'react-query';
import Breed from './components/Breed';
import Footer from './components/Footer';
import {BrowserRouter as Router, Route, Switch, useHistory, Link} from 'react-router-dom'

//Nisam korektno korisitio React router jer nije bilo moguce na nacin koji sam implementirao modal.
//Na većim stanicama bi implementirao rute koje pozivaju svoje odgovarajuće komponente no to ovdje nije imalo smisla s modalom.
//Na bivšim projektima sam korsitio react router, te sam upoznat s njegovom funkcionalnostima, pa sam ga tu koristio za manipulaciju urla.

const queryClient = new QueryClient()

export default function App() {
  return (
    <QueryClientProvider client={queryClient}>
      <DogBreedList />
    </QueryClientProvider>
  )
}

//upit za sve pasmine
function DogBreedList() {
  const { isLoading, error, data } = useQuery('repoData', () =>
    fetch('https://dog.ceo/api/breeds/list/all').then(res =>
      res.json()
    )
  )

  if (isLoading) return 'Loading...'

  if (error) return 'An error has occurred: ' + error.message;

  // pretovoriti data u array kako bi se kasnije map mogao korisiti
  const breeds = Array.from(Object.keys(data.message))

  return (
    <Router>
      <div className="bg_image">
      <h1 className='jumbotron'> ALL GOOD BOYS</h1>
      <div className="breeds">
        <Switch>
          <Route path="/" exact />
          <Route path="/:" />
        </Switch>
        <div className ='text-center'> {breeds.map((breed) => <Breed key={breed} breed={breed} podatci={data.message}/>)} </div>
      </div>
      <Footer />
      </div>
    </Router>
    )
}
